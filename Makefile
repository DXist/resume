NAME=Shigapov_Rinat
SOURCE=README.rst
TARGET=en/$(NAME)
SOFFICE ?= /Applications/LibreOffice.app/Contents/MacOS/soffice
SOFFICE_PARAMS:=$(TARGET).odt --outdir en "-env:UserInstallation=file:///tmp/LibO_Conversion" --invisible

all: en

odt: $(SOURCE)
	rst2odt.py --stylesheet=styles.odt --no-sections --create-links $(SOURCE) $(TARGET).odt

doc: odt
	$(SOFFICE) --convert-to doc $(SOFFICE_PARAMS)

docx: odt
	$(SOFFICE) --convert-to docx $(SOFFICE_PARAMS)

pdf: odt
	$(SOFFICE) --convert-to pdf $(SOFFICE_PARAMS)

en: odt pdf doc docx

rst2pdf:
	rst2pdf --stylesheets=rst2pdf.style --use-floating-images README.rst en/out.pdf
