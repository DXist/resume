==============
Rinat Shigapov
==============
.. class:: wrap
.. image:: rinat_shigapov_whitebackground.jpg
    :align: right
    :height: 160px
    :width: 160px

Location: Bishkek, Kyrgyzstan

Cell: +7 985 136 3880

Email: rinatshigapov@gmail.com

Github: https://github.com/DXist

Website: https://rustdev.pro/

LinkedIn: https://linkedin.com/in/rinatshigapov/

Telegram: dxist

RUST SOFTWARE ENGINEER / MACHINE LEARNING ENGINEER
==================================================

SUMMARY:
--------

* 12+ years of wide technical experience including but not limited to:

   * backend software development using Rust and Python, machine learning infrastructure
   * DevOps, CI, unit/end-to-end web/integration/migration testing
   * data processing and streaming applications

* Respectful of high quality work
* Interested in robust and secure software development,
  distributed systems, machine learning
* Curious about psychology, productivity, well-being
* Critical thinking viewpoint on chosen engineering tradeoffs

SKILLS:
-------

* Programming languages:            Rust, Python
* Development environment:          Linux, Git, Vim, Tmux
* Databases:                        PostgreSQL, MongoDB, RocksDB, MySQL
* Queues:                           Kafka, RabbitMQ
* Configuration management:         Ansible, Helm, Kustomize
* GitOps:	                        Flux/Flagger
* PaaS:                             Kubernetes
* Container engines:                Docker
* Cloud platforms:                  GCP, AWS
* Scalable ML research:             Polyaxon


--------------------------------------------------------------------------------

WORK EXPERIENCE:
----------------

Rust Software Engineer, BHFT, Remote, 04/24 - now
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*BHFT is an algorithmic trading company* https://bhft.com

* Development of trading platform infrastructure

   * Implemented policy-based authorization service

Rust Software Engineer, Eloe Inc, Remote, 08/23 - 10/23
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Eloe is an early stage credit card network* https://www.eloe.com/

* Development of replicated accounting ledger database with throughput target 100000+
  transactions/sec

   * memory is preallocated on startup
   * custom IO driver without memory allocations on IO path - `completeio`_. IO
     layer is swappable to enable reproducible simulations and fault injection to
     storage and network communication components.
   * deterministic `replication protocol`_

Rust Software Engineer, NXLog, Remote, 06/22 - 06/23
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*NXLog provides log collection solutions for multiple platforms and environments* https://nxlog.co/

* Design and development of an internal service for NXLog Cloud Platform

   * Suggested an idea of workflow-based fault-tolerant architecture
   * Developed an API and data access layers using Rocket framework and SQLx
     toolkit.
   * Suggested an idea of decentralized end-to-end authorization of microservice
     call chains

* Made active contributions to SQLx Toolkit:

   * Fixed `issue with unclosed unnamed PostgreSQL portal`_
   * Proposed `concurrency safe API layer`_ that handles transaction retries

Lead Software engineer, RingCentral, Moscow, Russia, 08/20 - 05/22
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*RingCentral is a provider of cloud-based communications and collaboration solutions for businesses* https://ringcentral.com

* Suggested to use GitOps + canary deployment approach based on Flux/Flagger
* Experimented with ML training infrastructure for speech recognition service

  * Streaming inference service for language detection - Rust, Tokio, Triton

    * Tool for inference validation and hyperparameter optimization - Rust.

    * Tools for preparation of multi language dataset containing audio and transcripts
      from YouTube - Python, httpx, asyncio

* Developed Rust based speech recognition and Meeting Notes backend services

   * RTP streams synchronization - Rust
   * data migrations - MongoDB
   * cold start speedup, K8s startup probes - Kafka

* Set up CI - Bazel, Docker, Gitlab, precommit, rustfmt

Software Engineer & Founder, ride pooling startup, Moscow, Russia, 03/21 - present
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* developing algorithms for ridepooling using Rust
* opensourced Rust implementation of two solvers of `weighted perfect matching
  problem`_

ML infrastructure engineer, Intelligence Retail, Moscow, Russia, 02/18 - 04/20
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*The company provides image recognition and analytics services to reduce retail audit costs* https://intrtl.com

* Built distributed image recognition infrastructure based on RabbitMQ and
  Kubernetes.
* Contributed `async inference interface`_ to mmdetection framework. Increased
  MaskRCNN inference throughput by ~17%
* Optimized recognition services in terms of Docker image size (~2.5x) and inference
  speed (~1.3x). Created asyncio-based asynchronous inference implementation.
* Initiated infrastructure migration to Google Cloud Platform. Decreased costs
  related to GPU resources in 4 times.
* Added `scale-to-zero`_ support to Kubernetes Horizontal Pod Autoscaler when using
  object/external metrics. Coordinated with SIG autoscaling and SIG apimachinery
  groups to merge changes to the upstream.
* Introduced reproducible deep learning platform based on
  Polyaxon
* Implemented business specific algorithms using Numba

Data Engineer, Conde Nast, Moscow, Russia, 02/17 - 02/18
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Multinational publisher of magazines like Vogue, Glamour, GQ, Tatler and their digital counterparts* https://www.condenast.ru/en/

* Architected Kafka based Data Management Platform (content storage, segmentation service, content based recommendation engine)
* Production hardening of prototypes provided by data scientists
* Built realtime clickstream data processing pipeline
* Mentored and shared good engineering practices with data scientists

Senior Software Engineer, Lamoda.ru, Moscow, Russia, 08/13 - 01/16
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*One of the biggest e-commerce fashion retailers in CIS* https://lamoda.ru

* Created stable backend service handling discount and loyalty program mechanics
* Built multi level CI pipeline that run unit, integration and migration tests under two platforms
* Developed multi environment configuration management tool based on Ansible
* Opensourced Django integration into Spyne RPC toolkit
* Organized metric data collection via Graphite
* Created concurrent token generation client using Golang
* Profiled and optimized service algorithms
* Become familiar with Debian packaging, created sbuild based sandbox integrated into Jenkins
* Enhanced logging subsystem
* Introduced Artifactory-based service deployment
* Automated release management


Software Engineer, Rambler Games, Moscow Russia, 08/11 - 05/13
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Digital ecosystem for gamers* https://kanobu.ru/

* Built digital distribution e-shop http://epic.kanobu.ru from the scratch
* Participated in daily meetings and did code review
* Coordinated work with desiner and HTML programmer
* Designed e-shop architecture on top of Django framework
* Implemented business logic, discount, bonus, shipment systems
* Integrated payment gates and suppliers through JSON and XML APIs
* Made framework for loading sample and test data with factory-boy
* Wrote automatic tests using unittest, mock and Selenium Web Driver
* Designed REST APIs for external services
* Prepared back office administration panel and report system
* Built client side application with Backbone.js
* Delegated long operations to Celery task queue
* Extended common projects and libraries

Software Engineer, Nevod, Perm, Russia, 10/08 - 05/10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Small integrator* http://www.nevod.ru/

* Developed web projects with Django, ExtJS
* Separated company infrastructure into OpenVZ containers
* Introduced infrastructure monitoring with Zabbix
* Incorporated automated backup and restore system based on Bacula
* Prepared corporate server solutions for clients using ALTLinux distributions
* Integrated client offices via openVPN
* Provided second level technical support for ALTLinux users at schools
* Maintained DNS and mail servers, web hosting

EDUCATION:
----------

* 2009	MS in Computer Science, State Technical University, Perm, Russia. GPA 5.0.
* 2008	Referent in Intercultural communications, Regional Management Center, Perm, Russia

COURSES:
--------

* 2019 Secure and Private AI (Udacity)
* 2019 Deep Learning in NLP (ipavlov.ai)
* 2018 Big Data Engineer (Otus.ru)
* 2018 Data Mining in Action (Applied Data Science Center)
* 2017 Mobile Startup School (Samsung & Strelka Vector)
* 2014 Technology Enterpreneurship (Novoed)
* 2013 Machine Learning (Coursera)
* 2013 Algorithms: Design and Analysis, Part 2 (Coursera)
* 2013 Algorithms: Design and Analysis, Part 1 (Coursera)
* 2013 Team Leader (kursrik.ru)

REFERENCES ARE AVAILABLE UPON REQUEST
-------------------------------------

.. _scale-to-zero:  https://github.com/kubernetes/kubernetes/pull/74526
.. _async inference interface: https://github.com/open-mmlab/mmdetection/pull/1647
.. _weighted perfect matching problem: https://crates.io/crates/sparse_linear_assignment
.. _issue with unclosed unnamed PostgreSQL portal: https://github.com/launchbadge/sqlx/pull/2081
.. _concurrency safe API layer: https://github.com/launchbadge/sqlx/issues/2082
.. _completeio: https://github.com/DXist/completeio
.. _replication protocol: https://pmg.csail.mit.edu/papers/vr-revisited.pdf
